window.validate = false;
window.unique_login = false;
window.unique_email = false;
window.unique_number = false;

$(document).ready(function() {

    $("#name").focus(function() {
		$("#name").css('border-color', '#6DC2FD');
	});
	$("#name").blur(function() {
		$("#name").css('border-color', '#C7C7C7');
		validation();
	});
	$("#number").focus(function() {
		$("#number").css('border-color', '#6DC2FD');
	});
	$("#number").blur(function() {
		$("#number").css('border-color', '#C7C7C7');
		validation();
	});
	$("#login").focus(function() {
		$("#login").css('border-color', '#6DC2FD');
	});
	$("#login").blur(function() {
		$("#login").css('border-color', '#C7C7C7');
		validation();
	});
	$("#email").focus(function() {
		$("#email").css('border-color', '#6DC2FD');
	});
	$("#email").blur(function() {
		$("#email").css('border-color', '#C7C7C7');
		validation();
	});
	$("#password1").focus(function() {
		$("#password1").css('border-color', '#6DC2FD');
	});
	$("#password1").blur(function() {
		$("#password1").css('border-color', '#C7C7C7');
		validation();
	});
	$("#password2").focus(function() {
		$("#password2").css('border-color', '#6DC2FD');
	});
	$("#password2").blur(function() {
		$("#password2").css('border-color', '#C7C7C7');
		validation();
	});

});



function validation () {

	var name = $("#name").val();
	var login = $("#login").val();
	var email = $("#email").val();
	var password1 = $("#password1").val();
	var password2 = $("#password2").val();
	var number = $('#number').val();
	var validation_name = false;
	var validation_login = false;
	var validation_email = false;
	var validation_password = false;

	
	//Work with login --------------------------------
	if (login.match(/\w+/g) == null && login.length != 0) {//проверка на лотинские символы и длину строки
		$("#error-login-correct").show('normal');
	}
	else {
		$("#error-login-correct").hide('normal');
		validation_login = true;
	}
	if (login.length === 0) { 
		$("#error-login-long").hide("normal");
		validation_login = false;
	}
	else {
		if (login.length < 3 || login.length > 10) {// проверка на кол. символов 
		$("#error-login-long").show("normal");
		validation_login = false;
		}
		else {
		$("#error-login-long").hide("normal");	
		}
	}
	if (validation_login == true) {
		$("#login").css('border-color','#3CFF0F');
	}
	else {
		$("#login").css('border-color','#F52424');
		$('#error-login-unique').hide('normal');
	}

	//------------------------------------------------

	//Work with password -----------------------------

	if (password1.length > 6 && password1.length != 0) { // Если длина не равна нулю.
		if (password1 == password2) {
		$("#password1").css('border-color','#31F531');
		$("#password2").css('border-color','#31F531');
		$("#error-password1").hide("normal");
		$("#error-password2").hide("normal");
		$("#error-password-small").hide("normal");
		validation_password = true;
		}
		else {
		$("#error-password1").show("normal");
		$("#error-password2").show("normal");
		$("#error-password-small").hide("normal");
		$("#password2").css("border-color","#FF3939");
		$("#password1").css("border-color","#FF3939");
		}
    }
    else {
    	$("#password1").css('border-color', '#FF3939');
    	$("#password2").css('border-color', '#FF3939');
    	$("#error-password-small").show("normal");
    	$("#error-password1").hide("normal");
    	$("#error-password2").hide("normal");
    }
    // Work with unic_number-------------------------
    if (number.length > 5 && number.length < 10 && number.length != 0) {
    	$('#error-number').hide('normal');
    	unique_number = true;
    }
    else {
    	$('#error-number').show('normal');
    	unique_number = false;
    }

    //-----------------------------------------------
    // work with Email ------------------------------
    email = email.match(/[^\d\sA-Z]/gi);
    if (email != null) {
    	if (email.length != 2) {
		$('#error-email-correct').show('normal');
		}
		else {
		$('#error-email-correct').hide('normal');
		validation_email = true;
		}
    }
    else {
    	$("#email").css('border-color','#F52424');
    }
    

	if (validation_email == true) {
		$("#email").css('border-color','#3CFF0F');
	}
	else {
		$("#email").css('border-color','#F52424');
	}
	//------------------------------------------------
	if (validation_login == true && validation_email == true && validation_password == true ) {
		validate = true;
	}
	else {
		validate = false;
	}
}

//------Ajax Login --------------------------------------
function funcBeforeLogin () {
	$('#login').hide('normal');
	$('.insert_ur_login').hide('normal');
	$(".loading-login").show();
}
function funcSuccessLogin ( data ) {
	$('#login').show('normal');
	$('.insert_ur_login').show('normal');
	$('.loading-login').hide();
	if (data == "false") {
		$('#error-login-unique').hide('normal');
		unique_login = true;
	}
	else {
		$('#error-login-unique').show('normal');
		unique_login = false;
	}
}


function ajaxLogin () {
	var login = $('#login').val();
	$.ajax({
		url: "php/check_any.php",
		type: 'POST',
		dataType: 'html',
		data: ({login: login}),
		beforeSend: funcBeforeLogin,
		success: funcSuccessLogin
	});
}
//---------------------------------------------
//----Ajax Email-------------------------------

function funcBeforeEmail () {
	$('#email').hide('normal');
	$('.insert_ur_email').hide('normal');
	$(".loading-email").show();
}
function funcSuccessEmail ( data ) {
	$('#email').show('normal');
	$('.insert_ur_email').show('normal');
	$('.loading-email').hide();
	if (data == "false") {
		$('#error-email-unique').hide('normal');
		unique_email = true;
	}
	else {
		$('#error-email-unique').show('normal');
		$('#email').css('border-color','#FF0000');
		unique_email = false;
	}
}


function ajaxEmail () {
	var email = $('#email').val();
	$.ajax({
		url: "php/check_any.php",
		type: 'POST',
		dataType: 'html',
		data: ({email: email}),
		beforeSend: funcBeforeEmail,
		success: funcSuccessEmail
	});
}
//------------------------------------------------
//-------AjaxCheckNumber--------------------------
function funcBeforenumber () {
	$('#number').hide('normal');
	$('.insert_ur_number').hide('normal');
	$(".loading-number").show();
}
function funcSuccessnumber ( data ) {
	$('#number').show('normal');
	$('.insert_ur_number').show('normal');
	$('.loading-number').hide();
	if (data == "false") {
		$('#error-number-unique').hide('normal');
		$('#number').css('border-color','#00FF24');
		unique_number = true;
	}
	else {
		$('#error-number-unique').show('normal');
		$('#number').css('border-color','#FF0000');
		unique_number = false;
	}
}


function ajaxnumber () {
	var number = $('#number').val();
	$.ajax({
		url: "php/check_any.php",
		type: 'POST',
		dataType: 'html',
		data: ({number: number}),
		beforeSend: funcBeforenumber,
		success: funcSuccessnumber
	});
}
//------------------------------------------------

$('#submit').click(function(event) {
	ajaxLogin();
	ajaxEmail();
	ajaxnumber();
}); 
		


//-------------------------------------------------


	$('form').submit(function(event) {

	if (validate == true && unique_login == true && unique_email == true && unique_number == true ) {
	return true;
	}
	else {
	return false;
	}	
	});