if (privileges=="admin") {
  $('#admin').show();
  $('#user').hide();
  $('#librarian').hide();
  $('#reg').hide();
};
if(privileges=="user") {
  $('#admin').hide();
  $('#user').show();
  $('#librarian').hide();
  $('#reg').hide();
}
if (privileges=="librarian") {
  $('#admin').hide();
  $('#user').hide();
  $('#librarian').show();
  $('#reg').hide();
}