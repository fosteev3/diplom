/**
 * Created by fost on 01.06.2016.
 */
$(document).ready(function () {
    console.log('Функция запущена');
    funRunAjax();
});

function funcBefore() {
    $('.loading_for_directory').show();
    $('body').css('cursor', 'progress');
    console.log('before');
}

function funcSuccess(data) {
    $('body').css('cursor', 'default');
    $('.loading_for_directory').hide();
    $('.catalog').show('normal');
    data = $.parseJSON(data);
    console.log(data);
    var name = new Array();
    var quantity = new Array();
    $.each(data, function (i, v) {
        name[i] = v.name;
        quantity[i] = Number(v.quantity);//преобразование в число
    });
    console.log('вывод в скрипте:');
    console.log(name);
    console.log(quantity);
    graph(name,quantity);

}

function funRunAjax() {
    var nameTable = "books_statistic";
    console.log('отправлена');
    $.ajax({
        url: "php/table_json.php",
        type: 'POST',
        dataType: 'html',
        data: ({nameTable: nameTable}),
        beforeSend: funcBefore,
        success: funcSuccess
    });
}

