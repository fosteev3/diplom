window.nameTable = null;
$(document).ready(function(){
  $(".table-table").hide();
  funRunAjax();
});
function funcBefore () {
	$('.loading_for_directory').show();
	$('.table-table').hide();
}

function funcSuccess ( data ) {

	$('.loading_for_directory').hide();
	$(".table-table").show();
	data = $.parseJSON(data);
	$('.table-table').html('<tr><td>Номер УДК</td><td>Название</td></tr>');
	$.each(data, function (i,v){
		v.number = v.number.toString();
		$(".table-table").append("<tr onClick='takeTr("+v.number+");'><td id='udk_number'>"+v.number+"</td><td>"+v.name+"</td></tr>")

	});
	
}
function funRunAjax (nameTable) {
	if (nameTable == null) {
		nameTable = "start";
	}
	$.ajax({
		url: "udk/php/udk-json.php",
		type: 'POST',
		dataType: 'html',
		data: ({nameTable:nameTable}),
		beforeSend: funcBefore,
		success: funcSuccess
	});
}
function takeTr (i) {
	i=i.toString();
	funRunAjax(i);
}
