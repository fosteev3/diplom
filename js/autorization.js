function funcBeforeAut () {
	$('.loading_aut').show('slow');
	$('#reg').hide();

}

function funcSuccessAut ( data ) {
	$('.loading_aut').hide('normal');
	$('#reg').show('fast');
	if (data == "false") {
	   $('#error-autorization').show('normal');
	}
	else {
		location.reload();
		$('#reg').hide('normal');
		/*var userName = data;
		if (userName != "admin") {
			$('#reg2').show('normal');
		}
		else {
			$('#reg3').show('normal');
		}*/
	}
}


function autorization () {
	var login = $('.login-login').val();
	var password = $('.login-password').val();
	$.ajax({
		url: "php/autorization.php",
		type: 'POST',
		dataType: 'html',
		data: ({login: login, password: password}),
		beforeSend: funcBeforeAut,
		success: funcSuccessAut
	});
}

$('#submit').click(function(event) {
  autorization();
});
$(document).keyup(function(event){
    if (event.keyCode == 13) {
        autorization();
    }
});
$("#aut").click(function () {
  $("#hiden").slideToggle("normal");
});