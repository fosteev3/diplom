$(document).ready(function () {
    funRunAjax();
});

function funcBefore() {
    $('.loading_for_directory').show();
    $('body').css('cursor', 'progress');
}

function funcSuccess(data) {
    $('body').css('cursor', 'default');
    $('.loading_for_directory').hide();
    $('.catalog').show('normal');
    data = $.parseJSON(data);
    $.each(data, function (i, v) {

        $('.content').append("<div class='content_block'> <h2>" + v.name + "</h2><img src='" + v.picture + "' alt='picture'><div class='title'><h4>Тематика:" + v.subjects + "</h4><h4>Авторы:" +
            v.authors + "</h4><h4>Год:" + v.years + "</h4></div><p>" + v.text + "</p><a onclick='takeA(" + v.UDK + ");' id='" + v.subjects + "' class='link-download' href='#'>Скачать</a></div>");
    });
}

function funRunAjax() {
    var nameTable = "books";
    $.ajax({
        url: "php/table_json.php",
        type: 'POST',
        dataType: 'html',
        data: ({nameTable: nameTable}),
        beforeSend: funcBefore,
        success: funcSuccess
    });
}

function takeA(i) {
    console.log('число полученное:');
    console.log(i);
    funRunStatistic(i);
}


function funRunStatistic(tematic) {
    console.log('Число отправляемое на сервер:');
    console.log(tematic);
    $.ajax({
        url: "php/books_statistic.php",
        type: 'POST',
        dataType: 'html',
        data: ({tematic: tematic}),
        beforeSend: funcBeforeQuantity,
        success: funcSuccessQuantity
    });
}

function funcSuccessQuantity(data2) {
    $('body').css('cursor', 'default');

    data2 = $.parseJSON(data2);
    console.log(data2);
}

function funcBeforeQuantity() {
    $('body').css('cursor', 'progress');
}