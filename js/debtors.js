$('#update').click(function(){
    Ajax();
});

function funcBefore () {
    $('#update').hide();
    $('.loading_for_directory').show();
}

function funcSuccess (data) {

    console.log('answer');
    console.log(data);
    data = $.parseJSON(data);
    var sum = 0;
    $.each(data, function (i,v)
    {
        sum++;
    });

    AjaxNext(sum);

}


function Ajax() {
    var nameTable = 'pay_base';
    $.ajax({
        url: "php/table_json.php",
        type: 'POST',
        dataType: 'html',
        data: ({nameTable:nameTable}),
        beforeSend: funcBefore,
        success: funcSuccess
    });
}



function funcBeforeNext () {
}

function funcSuccessNext ( data2 ) {
    console.log(data2);
    AjaxFinal();
}


function AjaxNext(sum) {
    console.log(sum);
    $.ajax({
        url: "php/debtors_data.php",
        type: 'POST',
        dataType: 'html',
        data: ({sum:sum}),
        beforeSend: funcBeforeNext,
        success: funcSuccessNext
    });
}

function funcBeforeFinal () {
}

function funcSuccessFinal ( data3 ) {
    $('.loading_for_directory').hide();
    console.log('Вывод количества и даты');
    data3 = $.parseJSON(data3);
    console.log(data3);
    var quantity = new Array();
    var date = new Array();
    $.each(data3, function (i,v) {
        quantity[i] = v.quantity;
        date[i] = v.date;
    });
    console.log(quantity);
    console.log(date);
    graph(quantity,date);
}


function AjaxFinal() {
    var nameTable2 = 'debtors_data';
    $.ajax({
        url: "php/table_json.php",
        type: 'POST',
        dataType: 'html',
        data: ({nameTable:nameTable2}),
        beforeSend: funcBeforeFinal,
        success: funcSuccessFinal
    });
}

