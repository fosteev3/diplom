/**
 * Created by fost on 01.06.2016.
 */
$('.search_button').click(function(){
    funRunAjax();
});

function funcBefore() {
    $('body').css('cursor', 'progress');
    $('.loading_for_directory').show();
    $('.content').html("<p></p>")

}

function funcSuccess(data) {
    $('body').css('cursor', 'default');
    $('.loading_for_directory').hide();
    $('.content').html("<p></p>")
    data = $.parseJSON(data);
    if(data == null) {
        $('.content').append("<div class='content_block'><h2>По вашему запросу ничего не найдено.</h2></div>");
    }
    else {
        console.log(data);
        $.each(data, function(i, data) {
            $('.content').append("<div class='content_block'> <h2>" + data.name + "</h2><img src='" + data.picture + "' alt='picture'><div class='title'><h4>Тематика:" + data.subjects + "</h4><h4>Авторы:" +
                data.authors + "</h4><h4>Год:" + data.years + "</h4></div><p>" + data.text + "</p><a onclick='takeA(" + data.UDK + ");' id='" + data.subjects + "' class='link-download' href='#'>Скачать</a></div>");
        });
    }


}

function funRunAjax() {
    var name = $('#checkbox_name').prop("checked");
    var author = $('#checkbox_author').prop("checked");
    var text = $('#checkbox_text').prop("checked");
    console.log('Имя:');
    console.log(name);
    console.log('Author');
    console.log(author);
    console.log('text');
    console.log(text);
    var searchString = $('.stroka').val();
    $.ajax({
        url: "php/search_result.php",
        type: 'POST',
        dataType: 'html',
        data: ({string: searchString,name:name,text:text}),
        beforeSend: funcBefore,
        success: funcSuccess
    });
}

