$("#run_ajax").click(function () {	
    funRunAjaxCAdd();
});
function funcBefore () {
	$('.podpanel_for_create_user').hide('normal');
	$('.loading-ajax').show('normal');
	$('body').css('cursor','progress');
}

function funcSuccess ( data ) {
	$('body').css('cursor','default');
	if (data=='true') {
		location.reload();
	}
	else {
		alert("Ошибка");
	}
	
}


function funRunAjaxCAdd () {
	var name = $('#name').val();
	var number = $('#number').val();
	var subjects = $('#subjects').val();
	var years = $('#years').val();
	var authors = $('#authors').val();
	var edition = $('#edition').val();
	var UDK = $(".select-tematic option:selected").val();
	var	file = 'img/books/example_book.png';
	var text = $('#text').val();
	console.log(text);

	$.ajax({
		url: "php/add_book.php",
		type: 'POST',
		dataType: 'html',
		data: ({name:name,number:number,subjects:subjects,years:years,authors:authors,
				edition:edition,UDK:UDK,file:file,text:text}),
		beforeSend: funcBefore,
		success: funcSuccess
	});
}
$('#years').blur(function(){
	var date = $('#years').val();
	if (date.length > 4) {
		$('#years').css('color','red');
	}
	else {
		$('#years').css('color','black');
	}
});