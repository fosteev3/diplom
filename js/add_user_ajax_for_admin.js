$("#run_ajax").click(function () {	
    funRunAjaxCAdd();
});
function funcBefore () {
	$('.podpanel_for_create_user').hide('normal');
	$('.loading-ajax').show('normal');
}

function funcSuccess ( data ) {
	$('.podpanel_for_create_user').show('normal');
	$('.loading-ajax').hide('normal');
	switch(data) {
		case 'login': 
		$('#error-login').show('normal');
		break;
		case 'password':
		$('#error-password').show('normal');
		break;
		case '!password':
		$('#error-!password').show('normal');
		break;
		case 'false':
		$('#false').show('normal');
		break;
		case 'true':
		location.reload();
		break;

	}
	
}


function funRunAjaxCAdd () {
	var name = $('#name').val();
	var number = $('#number').val();
	var login = $('#login').val();
	var email = $('#email').val();
	var password1 = $('#password1').val();
	var password2 = $('#password2').val();
	var status = $("#select_status :selected").html();
	$.ajax({
		url: "php/add_user.php",
		type: 'POST',
		dataType: 'html',
		data: ({name:name,number:number,login:login,email:email,password1:password1,password2:password2,status:status}),
		beforeSend: funcBefore,
		success: funcSuccess
	});
}
