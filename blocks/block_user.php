<img class="loading_aut" src="img/loading_for_aut.gif" height="100" width="100" alt="Загрузка">
<div class="reg" id="reg">
    <div id="showed">
        <form action="registration.html">
            <input class="registration" type="submit" value="Регистрация"></input>
        </form>
        <form>
            <input class="autorization" id="aut" type="button" value="Авторизация"></input>
        </form>
        <p align="center">
            Пожалуйста, <br>
            зарегистрируйтесь или авторизуйтесь <br>
            на сайте
        </p>
    </div>
    <div id="hiden">
        <form role="form" action="" method="post">
            <div>
                <p>Логин</p>
                <input name="login" class="login-login" type="text" placeholder="Введите ваш логин"></input>
            </div>
            <div class="pole_content">
                <p>
                    Пароль
                    <a class="forgot_password" href="">Забыли пароль?</a>
                </p>

                <input name="password" class="login-password" type="password" placeholder="Введите пороль"></input>
                <div id="error-autorization"><p>Логин или пароль не верны</p></div>
            </div>
            <input id="submit" class="login-action" type="button" name="submit" value="Войти"></input>
        </form>
    </div>
</div>
<div class="reg" id="user">
    <a class="col-users" href="librarian_pay.php"><p>Просмотр задолженностей</p></a>
    <a class="link-cancel" href="php/outLogin.php">Выйти</a>
</div>
<div class="reg" id="admin">
    <a class="col-users" href="http://localhost/phpmyadmin/"><p>Доступ к базе данных</p></a>
    <a class="link-cancel" href="php/outLogin.php">Выйти</a>
</div>
<div class="reg" id="librarian">

    <a class="col-users" href="librarian_pay.php"><p>Отдать книгу</p></a>
    <a class="col-users" href="librarian_pickup.php"><p>Забрать книгу</p></a>
    <a class="col-users" href="users.php"><p>Просмотр пользователей</p></a>
    <a class="col-users" href="books.php"><p>Просмотр книг</p></a>
    <a class="col-users" href="pay_base.php"><p>Должники</p></a>
    <a class="col-users" href="directory_udk.php"><p>Справочник УДК</p></a>
    <a class="col-users" href="books_graph.php"><p>Диаграмма скачивания</p></a>
    <a class="col-users" href="debtors_graph.php"><p>Диаграмма должников</p></a>
    <a class="link-cancel" href="php/outLogin.php"><p>Выйти</p></a>
</div>