<?php
session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Таблица книг</title>
</head>
<?php
require_once "blocks/block_head.php"
?>

<body>


<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>


<div class="content">
    <?php

    require_once "table/books-table.php";

    ?>
    <input id="add_user" class="login-action" action type="button" name="add_user" value="Добавить"></input>
</div>

<div class="panel">
    <div class="podpanel">
        <div class="head_add_book">
            <h2>Добавление книги</h2>
        </div>
        <p>Название книги</p>
        <input name="name" id="name" class="pole" type="text" placeholder="Введите название книги"></input>

        <p>Тематика</p>
        <input name="subjects" id="subjects" class="pole" placeholder="Введите тематику книги"></input>

        <p>Год выпуска</p>
        <input name="years" id="years" class="pole" type="number"></input>

        <p>Авторы</p>
        <input name="authors" id="authors" class="pole" placeholder="Перечислите авторов"></input>

        <p>Редакция</p>
        <input name="edition" id="edition" class="pole" placeholder="Укажите редакцию"></input>
        <p>Электронная версия книги</p>
        <input type="file" name="uploadfile" class="file">

        <div class="createUDK">
            <p>УДК</p> <br>
            <p class="mini">Выберите тематику из списка</p>
            <?php
            require_once "udk/php/udk-table1.php"
            ?>
        </div>
        <p>Описание</p>
        <textarea id="text" name="text" class="pole" cols="30" rows="10"></textarea>
        <a class="link-books" href="#">Отменить</a>
        <input class="button-books" type="submit" name="submit" id="run_ajax"></input>
    </div>
    <div class="loading-ajax"><img src="img/loading.gif" height="64" width="60" alt=""></div>
</div>

<div class="change">
    <div class="podpanel">
        <p>Название книги</p>
        <input name="name" id="name" class="pole" type="text" placeholder="Введите название книги"></input>

        <p>Тематика</p>
        <input name="subjects" id="subjects" class="pole" placeholder="Введите тематику книги"></input>

        <p>Год выпуска</p>
        <input name="years" id="years" class="pole" type="number"></input>

        <p>Авторы</p>
        <input name="authors" id="authors" class="pole" placeholder="Перечислите авторов"></input>

        <p>Редакция</p>
        <input name="edition" id="edition" class="pole" placeholder="Укажите редакцию"></input>

        <div class="createUDK">
            <p>УДК</p> <br>
            <p class="mini">Выберите тематику из списка</p>
            <?php
            require_once "udk/php/udk-table1.php"
            ?>
        </div>
        <a class="link-books" href="#">Отменить</a>
        <input class="button-books" type="submit" name="submit" id="run_ajax"></input>
    </div>
</div>


</body>
<script type="text/javascript">
    $('#add_user').click(function (event) {
        $('.panel').show('normal');
    });

    $('.link-cancel').click(function (event) {
        $('.panel').hide('normal');
    });
</script>
<script type="text/javascript">
    var privileges = '<?php echo($_SESSION['privileges']); ?>';
    $('.link-books').click(function () {
        $('.panel').hide('normal');
    });
</script>

<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript" src="js/delete_tr_table.js"></script>
<script type="text/javascript" src="js/change_user.js"></script>
<script type="text/javascript" src="js/add_books_ajax_for_admin.js"></script>
<script type="text/javascript" src="udk/js/udk_search_table.js"></script>


</html>