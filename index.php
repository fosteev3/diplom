<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Главная</title>
</head>
<?php
require_once "blocks/block_head.php"
?>
<body>

<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>
<script>
    $(document).ready(function () {
        $('#home').addClass('selected');
    });
</script>

<div class="content">
    <div class="paper">
        <h2>Добро пожаловать!</h2><br><br>
        <p>Вы зашли в подсистему мониторинга автоматизации библиотечного каталога, для того чтобы
            воспользоваться функциональными возможностям данной подсистемы вам необходимо авторизоваться или
            зарегистрироваться на сайте.</p><br>

        <p>Подсистема мониторинга автоматизации библиотечного каталога реализует учет
            вводимой информации; ведение баз данных; обеспечивает текущий контроль
            системного каталога вновь поступающей книжной продукции; контроль учетных
            записей пользователей.</p>
        <div class="photo">
            <<img src="img/example_statistic.png">
            <img src="img/example_books.png">
        </div>
        <p>Подсистема библиотечного учета фонда библиотеки,
            призвана автоматизировать и облегчить работу его персонала.
            С помощью современных технологий можно производить мониторинг
            информационных ресурсов библиотеки. </p> <br><br>

    </div>
</div>
</body>

<?php
require_once "blocks/block_footer.php";
?>
<script type="text/javascript" src="js/autorization.js"></script>

<script type="text/javascript">
    var privileges = '<?php echo($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
</html>