<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Диаграмма должников</title>
</head>
<?php
require_once "blocks/block_head.php"
?>

<body>

<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>
<div class="content">
    <div id="chartContainer" style="height: 400px; width: 100%;"></div>
</div>
<button id="update" class="login-action">Построить диаграмму</button>

<img src="img/loading_for_directory.gif" class="loading_for_directory" style="margin-left: 35%;margin-top: 10%; display: none;">
</body>

<script type="text/javascript" src="js/autorization.js"></script>
<script type="text/javascript" src="js/debtors.js"></script>
<script type="text/javascript">
    var privileges = '<?php echo ($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>

<script type="text/javascript" src="plugins/canvasjs-1.8.1-beta2/canvasjs.min.js"></script>
<script>
    function graph(quantity,date) {
        var i;
        console.log('graph:');
        for (i = 0; i < 3; i++) {
            console.log(date[i]);
        }
        console.log(quantity,date);
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Диаграмма должников"
            },
            data: [{
                type: "spline",
                dataPoints: [
                    {y: 10, label: '01.06.2016'},
                    {y: 20, label: '08.06.2016'},
                    {y: 30, label: '11.06.2016'},
                    {y: 35, label: '13.06.2016'},
                    {y: 40, label: '14.06.2016'},
                    {y: 35, label: '15.06.2016'},
                    {y: 30, label: '16.06.2016'},

                ]
            }]
        });
        chart.render();
    }

</script>
</html>