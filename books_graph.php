<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Диаграмма скачиваний</title>
</head>
<?php
require_once "blocks/block_head.php"
?>

<body>

<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>
<img src="img/loading_for_directory.gif" class="loading_for_directory"
     style="margin-left: 35%;margin-top: 10%; display: none;">


<div class="content">
    <div id="chartContainer" style="height: 400px; width: 100%;"></div>
</div>

</body>
<img src="" alt="">
<?php
require_once "blocks/block_footer.php"
?>


<script type="text/javascript" src="js/autorization.js"></script>
<script type="text/javascript">
    var privileges = '<?php echo($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript" src="js/json_graph.js"></script>
<script type="text/javascript" src="plugins/canvasjs-1.8.1-beta2/canvasjs.min.js"></script>
<script type="text/javascript">
    window.onload = function () {

    }
    function graph(name, quantity) {
        console.log('вывод в функции graph');
        console.log('вывод квантити');
        console.log(quantity);
        console.log('вывод имя');
        console.log(name);
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Диаграмма скаченных книг"
            },
            data: [{
                type: "column",
                dataPoints: [
                    {y: quantity[0], label: name[0]},
                    {y: quantity[1], label: name[1]},
                    {y: quantity[2], label: name[2]},
                    {y: quantity[3], label: name[3]},
                    {y: quantity[4], label: name[4]},
                    {y: quantity[5], label: name[5]},
                    {y: quantity[6], label: name[6]},
                    {y: quantity[7], label: name[7]},
                    {y: quantity[8], label: name[8]},
                    {y: quantity[9], label: name[9]},
                    {y: quantity[10], label: name[10]},
                    {y: quantity[11], label: name[11]},
                    {y: quantity[12], label: name[12]},
                    {y: quantity[13], label: name[13]},
                    {y: quantity[14], label: name[14]},
                    {y: quantity[15], label: name[15]},
                    {y: quantity[16], label: name[16]},
                    {y: quantity[17], label: name[17]},
                    {y: quantity[18], label: name[18]},
                    {y: quantity[19], label: name[19]},
                    {y: quantity[20], label: name[20]},
                    {y: quantity[21], label: name[21]},
                    {y: quantity[22], label: name[22]},
                    {y: quantity[23], label: name[23]},
                    {y: quantity[24], label: name[24]},
                ]
            }]
        });
        chart.render();
    }
</script>
</html>