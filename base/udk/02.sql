-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 21 2016 г., 23:32
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `udk`
--

-- --------------------------------------------------------

--
-- Структура таблицы `02`
--

CREATE TABLE IF NOT EXISTS `02` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `02`
--

INSERT INTO `02` (`id`, `number`, `name`, `text`) VALUES
(1, '21', 'Естественное богословие. Внеконфессиональная теодицея ', ''),
(2, '22', 'Библия', ''),
(3, '23', 'Христианство', ''),
(4, '23', 'Догматическое богословие', ''),
(5, '24', 'Практическое богословие	', ''),
(6, '25', 'Пастырское богословие. Благочестие', ''),
(7, '26', 'Христианская церковь в целом	', ''),
(8, '27', 'История христианской церкви в целом', ''),
(9, '28', 'Христианские церкви, общины и секты', ''),
(10, '29', 'Нехристианские религии', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
