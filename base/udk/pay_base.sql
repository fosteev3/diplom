-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 21 2016 г., 23:46
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `diplom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pay_base`
--

CREATE TABLE IF NOT EXISTS `pay_base` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nameUser` varchar(32) NOT NULL,
  `nameBook` varchar(32) NOT NULL,
  `numberUser` varchar(32) NOT NULL,
  `numberBook` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `pay_base`
--

INSERT INTO `pay_base` (`id`, `nameUser`, `nameBook`, `numberUser`, `numberBook`) VALUES
(1, 'Тест регистрации3', 'Математика для самых маленьких', '1111', '1111'),
(2, 'Тест регистрации3', 'Математика для самых маленьких', '1111', '1111'),
(3, 'Тест регистрации3', 'Математика для самых маленьких', '1111', '1111'),
(4, 'Виталий Александрович Золотько', 'Математика для самых маленьких', '45135113', '1111');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
