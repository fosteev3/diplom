<?php
/**
 * Created by PhpStorm.
 * User: fost
 * Date: 01.06.2016
 * Time: 12:47
 */
session_start();
echo $_REQUEST['data'];
?>
<!DOCTYPE html>
<html>
<head>
    <title>Результат поиска</title>
</head>
<?php
require_once "blocks/block_head.php"
?>

<body>

<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>
<img src="img/loading_for_directory.gif" class="loading_for_directory"
     style="margin-left: 35%;margin-top: 10%; display: none;">
<div class="search" id="search">
    <input type="search" id="stroka" class="stroka" placeholder="Искать">
    <input type="button" id="search_button" class="search_button" value="Поиск">
    <div class="checkboxes">
        <input type="checkbox" class="checkbox" id="checkbox_name">
        <label for="checkbox">Поиск по названию книги</label>
        <input type="checkbox" class="checkbox" id="checkbox_author">
        <label for="checkbox">Поиск по автору</label>
        <input type="checkbox" class="checkbox" id="checkbox_text">
        <label for="checkbox">Поиск по тексту</label>
    </div>
</div>
<div class="content"></div>
</div>
</body>
<?php
require_once "blocks/block_footer.php"
?>


<script type="text/javascript" src="js/autorization.js"></script>
<script type="text/javascript">
    var privileges = '<?php echo($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript" src="js/search.js"></script>
</html>
