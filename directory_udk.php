<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Справочник УДК</title>
</head>
<?php
  require_once "blocks/block_head.php"
?>

<body>

<?php
  require_once "blocks/block_header.php";
  require_once "blocks/block_user.php";
?>
<div class="table-udk">
	<table class='table-table' style='margin-left: 5%; margin-bottom: 5%; display: none;'>
	<tr>
	<td>Номер УДК</td>
	<td>Название</td>
	</tr>
	</table>
</div>
<img src="img/loading_for_directory.gif" class="loading_for_directory" style="margin-left: 35%;margin-top: 10%; display: none;">
</body>

<script type="text/javascript" src="js/autorization.js"></script>
<script type="text/javascript" src="js/directory_udk_ajax.js"></script>
<script type="text/javascript">
var privileges = '<?php echo ($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
</html>