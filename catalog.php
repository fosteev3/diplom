<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Каталог</title>
</head>
<?php
require_once "blocks/block_head.php"
?>

<body>

<?php
require_once "blocks/block_header.php";
require_once "blocks/block_user.php";
?>
<script>
    $(document).ready(function () {
        $('#catalog').addClass('selected');
    });
</script>

<img src="img/loading_for_directory.gif" class="loading_for_directory"
     style="margin-left: 35%;margin-top: 10%; display: none;">


<div class="content">
</div>

</body>
<img src="" alt="">
<?php
require_once "blocks/block_footer.php"
?>


<script type="text/javascript" src="js/autorization.js"></script>
<script src="js/catalog.js"></script>

<script type="text/javascript">
    var privileges = '<?php echo($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
<script src="js/books_statistic.js"></script>
</html>