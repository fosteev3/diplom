<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Таблица пользователей</title>
</head>
<?php
 require_once "blocks/block_head.php"
?>

<body>


<?php 
  require_once "blocks/block_header.php";
  require_once "blocks/block_user.php";
?>


<div class="content">
<?php
  require_once "table/users-table.php";
  
?>
<input id="add_user" class="login-action" action type="button" name="add_user" value="Добавить"></input>              
</div>

<div class="panel">
  <div class="podpanel">
   <p>Введите ваше имя</p>
   <input name="name" id="name" class="pole" type="text" placeholder="Введите имя"></input>
 
   <p class = "insert_ur_number">Введите ваш номер зачетки</p>
   <input name="number" id="number"  class="pole" type="number"  placeholder="Введите номер зачетки"></input>
 
   <p class = "insert_ur_login">Введите ваш логин</p>
   <input name="login" id="login"  class="pole" type="login"  placeholder="Введите логин"></input>

   <p class = "insert_ur_email">Укажите вашу электронную почту</p>
   <input name="email" id="email"  class="pole" type="email" placeholder="Введите email"></input>

   <p>Придумайте пароль</p>
   <input name="password1" id="password1" class="pole" type="password" placeholder="Введите пороль"></input>

   <p>Повторите пароль</p>
   <input name="password2" id="password2" class="pole" type="password" placeholder="Введите пороль"></input>

   <p class="privileges"><select size="1" id="select_status"  name="status">
   Выберите привелигированность
    <option disabled>Выберите роль</option>
    <option value="user">user</option>
    <option value="admin">admin</option>
    <option value="librarian">librarian</option>
   </select></p>
   <div id="error-login"><p>Логин от 3ех до 11 символов</p></div>
   <div id="error-password"><p>Пароль от 3ех до 16 символов</p></div>
   <div id="error-Nepassword"><p>Пароли не совпадают</p></div>
   <div id="false"><p>Ошибка на стороне сервера</p></div>
   <a class="link-cancel"  href="#" style="margin-left:15%">Отменить</a> 
   <input class="button-reg" type="submit"  name="submit" id="run_ajax"></input>
  </div>
  <div class="loading-ajax"><img src="img/loading.gif" height="64" width="60" alt=""></div> 
</div>





</body>
<script type="text/javascript">
$('#add_user').click(function(event) {
$('.panel').show('normal');
}); 
  
$('.link-cancel').click(function(event) {
  $('.panel').hide('normal');
}); 
</script>
<script type="text/javascript">
var privileges = '<?php echo ($_SESSION['privileges']); ?>';
</script>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript" src="js/delete_tr_table.js"></script>
<script type="text/javascript" src="js/change_user.js"></script>
<script type="text/javascript" src="js/add_user_ajax_for_admin.js"></script>


</html>